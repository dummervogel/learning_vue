﻿# Vue

[TOC]



## 简介

```html
<div id="app"><!--view-->
  <input type="text" v-model="username">
  <p>{{ username }}</p>
</div>
<script>
const vm = new Vue({ //配置对象
  el: '#app', //element选择器
  data: { //数据（model)
    username: 'Hello Vue!'
  }
});
</script>
<!--
MVVM:
model: 模型，数据对象（data）
view: 视图，模板页面
viewmodel：视图模型(vue的实例-const vm)
-->
```

使用input监听，将数据保存到username，通过数据绑定更新`<p>`。即代码中的vm

- 动态监听
- 数据绑定

view - viewmodel - model(->数据绑定，<- 动态监听 )

声明式开发（不需要管控制流程）

vs. 命令式开发(jquery)

```html
<script src="../js/vue.js"></script>
<script>
  new Vue({
      el: '#app',
      data: {
        msg: "i will be back",
        msg2: "<a href='ddd'>i will be back</a>",
        imgUrl: "https://xxx.png"
      },
      methods: {
          test() {
              alert('hehe!');
          },
          test2(content) {
              alert(content);
          }
      }
  })
</script>
<div id="app">
    <p>
  	{{msg}}  
	</p>
	<p>
	 {{msg.toUpperCase()}}
	</p>
	<p v-html="msg2"> <!--把文本理解为html-->
	</p>
	<p v-text="msg2">
	</p>
	<img v-bind:src="imgUrl"></img><!--强制绑定-->
	<img :src="imgUrl"></img><!--强制绑定2-->

	<button v-on:click="test">
   	 <!--绑定方法, v-on:click可替换为@click-->
    	<!-- 也可以传参数test('aa')-->
    	test1
	</button>

	<button v-on:click="test2('aa')">
    	<!-- 也可以传参数test('aa')-->
    	test2
	</button>
</div>

```

## 计算和监视

```html
<script>
 const vm = new Vue({
 	el: "#demo",
    data: {
        firstname: 'A',
        lastname: 'B'，
        fullname2: 'A B'
    },
    computed: {
        //什么时候执行：初始化显示/相关的data属性发生改变
        fullname() { //计算属性的方法，方法的返回值为属性值
            // 这样在html页面中，用v-model可以引用
            return this.firstname + this.lastname;
        }，
        // 可实现双向的更新
        fullname3: {
        	//回调：读取当前属性值更新
        	get() {
     			return this.firstname+" " +this.lastname;
 			},
            //回调：当属性值发生改变时
            set(value) {
               const names = value.split(' ');
               this.firstname= names[0];
               this.lastname = names[1];
            },
    	}
    }，
    watch: { //配置监视
    	//	firstname发生变化时触发
     	firstname: function(value) {
     		console.log(this); // this 即const vm
     		this.fullname2 = value + " " + this.lastname; //这样在html页面中，v-model为fullname2就会更新
 		} 
 	}
 });
 vm.$watch('lastname', function(value) {
     // 更新fullname2
     this.fullname2 = this.firstname + " " + value;
 });
</script>
```

两种方法，一种定义在vm里，一种是通过$。

```html
<p>
    {{fullname}}
</p>
<p>
    {{fullname}}
</p>
<p>
    {{fullname}}
</p>
```

只会调用一次fullname()，采用缓存技术。多次读取只会执行一次getter()。

## 强制绑定class和style

```html
<!--class和style可以动态改变-->
<script>
  new Vue({
      el: '#demo',
      data: {
          a: 'aClass',
          isA: true,
          isB: false,
          activeColor: 'red',
          fontSize: 20
      },
      methods: {
          update() {
            this.a = 'bClass';
            this.isA = false;
            this.isB = true;
            this.activeColor = 'green';
            this.fontSize = 30;
          };
      }
  });
</script>
<div id="demo">
    <!-- 可以同时有静态和动态class，相当于是合并的效果-->
    <p :class="a" class="cClass">
        
    </p>
    <!--如果是true，则为冒号前的元素会留下 -->
    <p :class="{aClass: isA, bClass: isB}">
        
    </p>
    <!--用的少-->
    <p :class="['aClass','cClass']">
        
    </p>
    <!--style绑定-->
    <p :style="{color: activeColor, fontSizer: fontSize + 'px'}">
        
    </p>
    <button @click="update">
        更新
    </button>
</div>
```



## 条件渲染

v-if

v-else

v-show

```html
<script>
  new Vue({
     el: '#demo',
     data: {
         ok: false
     }
  });
</script>
<div id="demo">
    <p v-if="ok">
        成功了
    </p>
    <p v-else="true">
        失败了
    </p>
    <p v-show="ok">
        表白成功
    </p>
    <p v-show="!ok">
        表白失败
    </p>
    <button @click="ok=!ok">
        切换
    </button>
</div>
```

v-show通过display:none来控制，v-if则是直接显示或移除标签。所以频繁切换用v-if也比较好。

## 列表渲染

```html
<script>
	new Vue({
       el: '#demo',
       data: {
           persons: [
               {name: 'Tome', age:18},
               {name: 'Jack', age:19}
           ]
       },
       methods: {
           deleteP(index) {
               // 虽然persons本身没有发生变化
               // 但是vue也有数组更新检测的方法
               // 如splice()除了更新数组外，也会强制更新界面
               
               this.persons.splice(index, 1);
           },
           updateP(index, newP) {
               // 下面操作并没有改变persons本身，
               // 而vue只监视persons改变
               // 因为vue的data中定义的是persons
      
               // this.persons[index] = newP;
               // 使用splice更新
               this.persons.splice(index, 1, newP);
           }
       }
    });
</script>
<!--遍历数组 -->
<ul>
    <li v-for="(p, index) in persons" :key="index">
    	{{index}}--{{p.name}}--{{p.age}}
        --<button @click="deleteP(index)">
          删除
        </button>
        --<button @click="updateP(index， {name:'cat', age:20})">
          更新
        </button>
    </li>
</ul>
<!--遍历对象 -->
<ul>
    <li v-for="(value,key) in persons[1]" :key="key">
      {{value}}--{{key}}
    </li>
</ul>
```

注意vue重写了<u>数组中改变数组内部数据的方法</u>（先调用原生，再更新界面）。

## 列表的搜索和排序

```html
<script>
  new Vue({
     el: '#test',
      data: {
          persons: [
               {name: 'Tome', age:18},
               {name: 'Jack', age:19}
           ],
          searchName: '',
          orderType: 0, //0：原本,1:升序,2:降序
      },
      computed: {
          filterPersons() {
              // 取出相关的数据
              const {searchName, persons， orderType} = this;
              let fPersons;
              // 对persons过滤
              fPersons = persons.filter(p => p.name.indexOf(searchName) >= 0);
              // 排序
              if(orderType != 0) {
                  fPersons.sort(function(p1, p2) {
                     // 1升序，2降序
                     if(orderType == 2) {
                       return p2.age - p1.age;
                     }
                     else {
                       return p1.age - p2.age;     
                     }
                  });
              }
              return fPersons;
          }
      },
      methods: {
          setOrderType(orderType) {
              this.orderType = orderType;
          }
      }
  });
</script>
<div id="test">
   <input type="text" v-model="searchName"/>
    <ul>
        <li v-for="(p, index) in filterPersons" :key="index">
          {{index}} -- {{p.name}} -- {{p.age}}
        </li>
    </ul>
    <button @click="setOrderType(1)">
        年龄升序
    </button>
    <button @click="setOrderType(2)">
        年龄降序
    </button>
    <button @click="setOrderType(0)">
        年龄原序
    </button>
</div>
```



## 事件处理

```html
<script>
  new Vue({
     el: '#example',
     methods: {
          test1() {
              alert("test1");
          },
          test2(msg) {
            alert(msg);
          },
          // 可以用默认的event参数
          test3(event) {
              alert(event.target.innerHTML);
          },
          test4(number, event) {
              alert(number + event.target.innerHTML);
          },
          test5() {
              alert("out");
          },
         // 停止事件冒泡
          test6(event) {
              //event.stopPropagation();
              // 防止触发test6后又触发test5
              // 也可以在@click修饰@click.stop
              alert("in");
          },
         // 停止事件默认行为
         test7(event) {
             // event.preventDefault();
             // 防止跳转到a href的链接地址
             // 也可以在@click修饰@click.prevent
             alert("点击了");
         },
         // 按键修饰符
         test8(event) {
             /*
             if(event.keyCode === 13) {
                 alert(event.target.value);
             }*/
             // 也可以在@keyup修饰@keyup.13直接过滤
             // 或者@keyup.enter
         }
     }
  });
</script>
<div id="example">
    <button @click="test1">
        test1
    </button>
    <button @click="test(sth)">
        test2
    </button>
    <button @click="test3">
        test3
    </button>
    <!--可强制传event参数-->
    <button @click="test4(123, $event)">
        test4
    </button>
    
    <div style="width: 200px; height: 200px; background: red" @click="test5">
        <div style="width: 100px; height: 100px; background: blue" @click="test6">
            
        </div>
    </div>
    <a href="http://www.baidu.com" @click="test7">去百度</a>
    <input type="text" @keyup="test8"></input>
</div>
```

## 表单数据的提交（自动收集）:v-model

1. text/textarea
2. checkbox
3. radio
4. select

```html
<div id="demo">
    <!--注意阻止默认行为 -->
    <form action="/xxx" @submit.prevent="handleSubmit">
        <input type="text" v-model="username"/>
        <input type="password" v-model="pwd"/>
        <!--单选框 -->
        <input type="radio" id="female" v-model="sex" value="女"/>
        <input type="radio" id="male" v-mode="sex" value="男"/>
        <!--多选框 -->
        <input type="checkbox" value="basket" id="basket" v-model="likes"/>
        <input type="checkbox" value="pingpong" id="pingpong" v-model="likes"/>
        <input type="checkbox" value="foot" id="foot" v-model="likes"/>
        <!-- 下拉框, 绑定默认的cityId-->
        <select v-model="cityId">
            <option value="">未选择</option>
            <option :value="{{city.id}}" v-for="(city, index) in allCities" :keys="index">{{city.name}}</option>
        </select>
        <!-- textarea -->
        <textarea rows="10" v-model="desc"></textarea>
    </form>
</div>
<script>
  new Vue({
     el: "#demo",
     data: {
         username: '',
         pwd: '',
         sex: '女',
         likes: ['foot'],
         allCities: [{id: 1, name:'BJ'},
                    {id: 2, name:"SH"}],
         cityId: 2, //默认是SH
         desc: ''
     },
     methods: {
         handleSubmit() {
             console.log(this.username, 
                         this.pwd,
                        this.sex,
                        this.likes,
                        this.cityId,
                        this.desc);
         }
     }
  });
</script>
```



## 生命周期

![lifecycle](lifecycle.png)

```html
<div id="test">
    <button @click="destroyVM()">
        destroy vm
    </button>
    <p v-show="isShow">
        尚硅谷
    </p>
</div>
<script>
  new Vue({
     el: "#test",
     data: {
         isShow: true
     },
     // 初始化显示之后，见上图
     mounted() {
         this.intervalId = setInterval(function() {
             this.isShow = !this.isShow;
         }, 1000);
     },
     methods: {
         destroyVM() {
             // 干掉vm
             // 但是setInterval会继续执行
             this.$destroy();
         }
     },
     beforeDestory() {
         // 清除定时器
         clearInterval(this.intervalId);
     }
  });
</script>
```

mounted()和beforeDestroy()使用的最多。

- mounted()：发送ajax请求，启动定时器
- beforeDestroy()：收尾工作，如清除定时器

现在内存替换，再更新页面。mount即为挂载到页面的操作。

Destroy()只是解除绑定状态，页面还在。

## Vue动画

操作css的transition或animation。

![transition](transition.png)

```html
<div id="test">
    
    <button @click="isShow=!isShow">
        toggle
    </button>
    <transition name="xxx">
      <p v-show="isShow">
          hello
      </p>
    </transition>
</div>

<div id="test2">
    
    <button @click="isShow=!isShow">
        toggle
    </button>
    <transition name="move">
      <p v-show="isShow" class='xxx'>
          hello
      </p>
    </transition>
</div>
<script>
    new Vue({
       el: '#test',
       data: {
           isShow: true
       }
    });
    
     new Vue({
       el: '#test2',
       data: {
           isShow: true
       }
    });
</script>
<!--定义样式，注意和p的xxx相对应-->
<style>
    /* 显示/隐藏的过渡效果*/
    .xxx-enter-active, .xxx-leave-active {
        transition: opacity 1s;
    }
    /*隐藏的样式*/
    .xxx-enter, .xxx-leave-to {
        opacity: 0;
    }
    
    .move-enter-active {
        transition: all 1s;
    }
    
    .move-leave-active {
        transition: all 3s;
    }
    
    .move-enter, .move-leave-to {
        opacity: 0;
        transform: tranlateX(20px); //向右移动
    }
    
</style>
```

加transition后，会在p标签里加类名class='v-enter','v-enter-to','v-enter-active'。

目标外包裹transition

定义class样式，transition，过渡的样式

还有一种是animation，用的少，可以见官方文档。

## 过滤器

### 显示格式化的日期时间

定义过滤器

```javascript
Vue.filter(filterName, function(value[,ag1,...]){
   return newvalue; 
});
```

使用过滤器

```html
<div>
    {{myData | filterName(arg)}}
</div>
```



Full example:

```html
<script src="moment.js"></script>
<script>
// 自定义过滤器
// 函数对象
Vue.filter('dateStr', function(value, format){
    // 注意即为format==undefined?"xxx":format
    return moment(value).format(format || "YYYY-MM-DD HH:mm:ss");
});
new Vue({
   el: '#test',
   data: {
       date: new Date()
   }
});
</script>

<div id="test">
    <p>
        {{date}}
    </p>
    <!--指定过滤器的名字 -->
    <p>
        {{date | dateStr}}
    </p>
</div>
```

## Vue指令

- v-on
- v-bind
- v-model：双向数据绑定
- ref：绑定一个标签，可在vue代码中引用
- v-cloak：防止闪现表达式，和css配合：

```html
<div id="example">
    <p ref="content">
        ddd
    </p>
    <p v-text="msg">
        
    </p>
    <!--在解析前，会应用[[v-cloak]]style，解析后v-cloak消失-->
    <p v-cloak="">
        {{msg}}
    </p>
    <button @click="hint">
        提示
    </button>
</div>
<script>
	new Vue({
       el: '#example',
       data: {
          msg: "ddd" 
       },
       methods: {
           hint() {
             alert(this.$refs.content.textContent);    
           }
       }
    });
</script>
<style>
    [v-cloak] {
        display: none;
    }
</style>
```

可以自定义指令

```html
<script>
// 定义全局指令,可以通过v-upper引用
Vue.directive('upper', function(el, binding) {
   // el: 所在的标签
   // binding: 包括指令名，表达式值等
   el.textContent = binding.value.toUpperCase();
});

new Vue({
   el: '#test1',
   data: {
      msg: "what id dfd"
   },
   // 当前Vue下的局部指令
   directives: {
       'lower': function(el, binding) {
           el.textContent = binding.value.toLowerCase();
       }
   }
});
    
new Vue({
   el: '#test1',
   data: {
      msg: "just do it"
   }
});
</script>
<div id="test1">
    <p v-upper="msg">
        
    </p>
    <p v-lower="msg">
        
    </p>
</div>
<div id="test2">
    <p v-upper="msg">
        
    </p>
    <p v-lower="msg">
        
    </p>
</div>

```

## Vue插件

```javascript
/*
Vue插件库
*/
(function() {
   //向外暴露的插件对象
   const MyPlugin = {};
   //插件必须要有install()
   MyPlugin.install = function(Vue, options) {
       // 添加全局的方法或属性
       Vue.myGlobalMethod = function() {
           console.log("vue函数对象的方法")
       }
       // 添加全局指令
       Vue.directive('my-directive', function(el,binding) {
          el.textContent = binding.value; 
       });
       // 添加实例方法
       Vue.prototype.$myMethod = function() {
           console.log("vue实例对象方法");
       }
   }
   // 向外暴露插件
   window.myplugin = MyPlugin;
})();
```

使用

```html
<script src="vue.js"></script>
<script src="myplugin.js"></script>
<script>
  Vue.use(myplugin); //内部会执行MyPlugin.install(Vue)
  Vue.myGlobalMethod();
  const vm = new Vue({
     el: '#test',
     data: {
         msg: 'i love you'
     }
  });
  vm.$myMethod();
</script>
<div id="test">
    <p v-my-directive="msg">
        
    </p>
    
</div>
```

## Vue Cli创建项目

vue-cli是官方提供的

```bash
npm install -g vue-cli
vue init webpack vue_demo
cd vue_demo
npm install
npm run dev
```

## Vue基于脚手架编写项目

- buid 配置

  - webpack: 注意port，autoOpenBrowser

- config：

- src

  - main.js：入口文件，在webpack.base.conf里面配置

- static：全局图片

- .babelrc：ES6转ES5

- .eslintrc：

- .eslintignore：忽略哪些文件检查

- index.html：主页

- package.json：生命依赖

- README.md

   

在src的根目录下创建App.vue

```vue
<template>
	<div>
        
    </div>
</template>
<script>

</script>
<style>
</style>
```

其它组件放在src/components下。比如HelloWorld.vue

```vue
<template>
	<div>
        
    </div>
</template>
<script>
	// 向外暴露
    export default {
        
    }
</script>
<style>
</style>
```

所有的vue均为此模板

比如，填写

```vue
<template>
	<div>
        <p class="msg">
           {{msg}}    
        </p>
    </div>
</template>
<script>
	// 向外暴露
    export default { //配置对象（与vue一致）
        data() { //必须写函数
            return {
                msg: 'Hello Vue'
            }
        }
    }
</script>
<style>
    .msg {
        color: red;
    }
</style>
```

然后在App.vue中引用

```vue
<template>
	<div>
        <img class="logo" src="./assets/logo.png" alt="logo"/>
        <!--3.使用组件标签-->
        <HelloWorld/>
    </div>
</template>
<script>
    // 1.引入组件
	import HelloWord from './components/HelloWorld.vue'
    export default {
        // 2. 映射组件标签
        components: {
            HelloWorld
        }
    }
</script>
<style>
    .logo {
        width: 200px;
        height: 200px;
    }
</style>
```

在main.js中

```javascript
/*
入口JS:创建vue实例
*/
import Vue from 'vue';
import App from './App.vue';

new Vue({
   el:'#app',
   components: {
       App
   },
   template: '<App/>'
});
```

## 打包发布

```bash
npm run build
```

生成在dist

### 发布1：使用静态服务器工具包

```bash
npm i -g serve
serve dist
```

### 发布2：使用动态web服务器(tomcat)

修改配置：webpack.prod.conf.js

```javascript
output: {
    publicPath: '/xxx/' //打包文件夹名称
}
```

重新打包

```bash
npm run build
```

修改dist文件夹名称：xxx

将xxx拷贝到tomcat的webapps目录下

访问：http://localhost:8080/xxx

## eslint编码规范检查

可以在.eslintrc.js上配置规则。

在.eslintignore忽略检查文件

## Vue案例

### 初始化显示

组件化编程思路

1. 拆分组件
2. 静态组件
3. 动态组件
   1. 初始化显示
   2. 交互

![splitexample](splitexample.png)

在src创建App.vue,main.js

在components创建Add.vue, List.vue, Item.vue

main.js

```javascript
import Vue from 'vue';
import App from './App.vue';

new Vue({
   el:'#app',
   components: {
       App
   },
   template: '<App/>'
});
```

在static/css下放入bootstrap.css

在index.html下引用css

在App.vue下编写

```vue
<template>
	<div>
        <header class="site-header jumbotrdn">
            <div class="container">
                <div class="row">
                    <div clas="col-xs-12">
                        <h1>
                            请发表vue的评论    
                        </h1>    
                    </div>    
                </div>    
            </div>
        </header>
        <div class="container">
            <!-- 传入了一个函数-->
            <Add :addComment="addComment"/>
            <List :comments="comments" :deleteComment="deleteComment"/>
        </div>
    </div>
</template>
<script>
	import Add from './components/Add.vue';
    import List from './components/List.vue';
    export default {
        data() {
            return {
                //数据在哪个组件，更新数据的行为就定义在哪个组件
                comments: [
                    {
                        name: '8dd',
                        content: 'vue还不错'
                    },
                    {
                        name: 'cat',
                        content: 'vue so easy'
                    }
                ]
            }
        },
        methods: {
            addComment(comment) {
                this.comments.unshift(comment);
            }，
            deleteComment(index){
                this.comments.splice(index, 1);
            }
        }
        components: {
            Add,
            List
        }
    }
</script>
<style>
</style>
```

在Add.vue,List.vue的template下

List.vue下

```vue
<template>
   <div class="col-md-8">
       <ul class="list-group">
           <Item v-for="(comment, index) in comments" :key="index" :comment="comment" :deleteComment="deleteComment" :index="index"/>    
       </ul>    
   </div>
</template>
<script>
    import Item from './Item.vue';
    export default{
        //声明接收属性, 这个属性就会成为组件对象的属性
        props: ['comments','deleteComment'], //只指定属性名
        components: {
            Item
        }
    }
</script>
```

在Item.vue下

```vue
<template>
    <p class="user">
        <span>{{comment.name}}</span>说:
        <span>{{comment.content}}</span>
    </p>
</template>
<script>
    export default {
        props: { //指定属性名和属性值的类型
           comment: Object
        }
    }
</script>
```

### 交互添加

```vue
<template>
<button @click="add">
        提交
</button>
<input type="text" v-model="name"/>
<input type="text" v-model="content"/>
</template>
<script>
    export default {
        props: {
            // 注意这里声明了一个函数
            // 制定了属性名/属性值和必要性
            addComment: {
                type: Function,
                required: true
            }
        }
        data() {
            return {
                name: '',
                content: ''
            }
        }
        methods: {
            add() {
                //检查输入的合法性
                const name = this.name.trim();
                const content = this.content.trim();
                if(!name || !content) {
                    alert("姓名或内容不为空");
                    return;
                }
                //根据输入的数据，封装成comment对象
                const comment = {
                    name,
                    content
                }
                // 添加到
                this.addComment(comment);
                // 清除
                this.name='';
                this.content='';
            }
        }
    }
</script>
```

### 交互删除

在Item.vue中添加

```vue
<template>
  <a href="javascript:;" @click="deleteItem">删除</a>
</template>
<script>
  export default{
      props: {
          comment: Object,
          deleteComment: Function,
          index: Number
      },
      methods: {
          deleteItem() {
              const {comment} = this;
              if(window.confirm('确定删除${comment.name}的评论吗？')) {
             deleteComment(index);     
          }
      }
  }
</script>
```

### 静态组件

![](static.png)

创建App.vue

```vue
<template>
  <div class="todo-container">
      <div class="todo-wrap">
        <TodoHeader/>
        <TodoList/>
        <TodoFooter/>
      </div>
      
  </div>
</template>
<script>
  import TodoHeader from './components/ToHeader.vue';
  import TodoList from './components/TodoList.vue';
  import TodoFooter from './components/TodoFooter.vue';
  export default {
      components: {
          TodoHeader,
          TodoList,
          TodoFooter
      }
  }
</script>
```

创建main.js

```javascript
import Vue from 'vue';
import App from './App.vue';

import './base.css';

new Vue({
   el: '#app',
   components: {App},
   template: '<App/>'
});
```

### 动态初始化显示

更新App.vue

```vue
<script>
	export default{
        data() {
            return {
                todos: [
                    {title: '吃饭', complete: false},
                    {title: '睡觉', complete: true},
                    {title: 'coding', complete: false}
                ]
            }
        }
    }
</script>
```

以及

```vue
<template>
  <div class="todo-container">
      <div class="todo-wrap">
        <TodoHeader/>
        <TodoList :todos="todos"/>
        <TodoFooter/>
      </div>
      
  </div>
</template>
```

todolist.vue

```vue
<script>
  import TodoItem from './TodoItem.vue'
  export default{
      props: {
          todos: Array
      },
      components: {
          TodoItem
      }
  }
</script>
<template>
  <ul class="todo-main">
        <TodoItem v-for="(todo, index) in tods" :key="index" :todo="todo" :index="index"/>
  </ul>
</template>
```

TodoItem.vue

```vue
<template>
  <li>
    <label>
      <input type="checkbox" v-model="todo.complete"/>
      <span>{{todo.title}}</span>
    </label>  
  </li>
</template>
<script>
  export default{
      prop: {
          todo: Object,
          index: Number
      }
  }
</script>
```

### 交互添加

TodoHeader.vue

```vue
<template>
  <div class="todo-header">
    <input type="text" placeholder="输入用户名称，回车确认" @keyup.enter="addItem" v-model="title"/>    
  </div>
</template>
<script>
  export default {
      data() {
          return {
              title: ''
          }
      },
      props: {
          // 声明接收App.vue的函数
          addTodo: Function
      }
      methods {
          addItem() {
              // 1.检查合法性
              const title = this.title.trim();
              // 2.生成todo
              if(!title) {
                  alert("必须输入");
                  return;
              }
              const todo = {
                  title,
                  complete: false
              }
              // 3.添加到todos
              this.addTodo(todo);
              // 4.清除输入
              this.title = '';
          }
      }
  }
</script>
```

对应的App.vue添加

```vue
<script>
  export default() {
      methods: {
          addTdo(todo) {
              this.todos.unshift(todo);
          }
      }
  }
</script>
<div>
    <TodoHeader :addTodo="addTodo"/>
</div>
```

### 交互删除

在TodoItem.vue增加

```vue
<template>
  <!--
  onmouseover, onmouseout: 从该元素进出内部元素，或者进出该元素都会触发
  onmouseenter, onmouseleave：只会在进出该元素触发
  -->
  <li @mouseenter="handleShow(true)" @mouseleave="handleShow(false)" :style="{background: bgColor}">
   <label></label>
   <button class="btn btn-danger" v-show="isShow" @click="deleteItem()">
       删除
   </button>
  </li>
</template>

<script>
  export default {
      data() {
          return {
            bgColor: 'white', //默认背景颜色
            isShow: false; //按钮是否默认显示
          }
      },
      props: {
          deleteTodo: Function
      }
      methods: {
          handleEnter(isEnter) {
              if(isEnter) {
                  this.bgColor = "#aaaaaa";
                  this.isShow = true;
              }
              else {
                  this.bgColor = "white";
                  this.isShow = false;
              }
          },
          deleteItem() {
              const {todo, index, deleteTodo} = this;
              if(window.confirm('确认删除${todo.title}?')) {
                  deleteTodo(index);
              }
          }
      }
  }
</script>
```

在App.vue增加

```vue
<script>
  export default() {
      methods: {
          deleteTodo(index) {
        	this.todos.splice(index, 1);
    	}
      }
  }
</script>
<div>
    <TodoList :addTodo="addTodo" :deleteTodo="deleteTodo"/>
</div>
```

在TodoList.vue接收

```vue
<script>
  export default {
      props： {
  	      deleteTodo: Function
      }
  }
</script>
<div>
    <TodoItem :deleteTodo="deleteTodo"/>
</div>
```

### 交互footer组件

TodoFooter.vue

```vue
<script>
  export default {
      props: {
          todos: Array,
          deleteCompleteTodos: Function,
          selectAllTodos: Function
      },
      computed: {
          completeSize() {
              return this.todos.reduce((preTotal, todo) => preTotal + (todo.complete?1:0), 0);
          },
          isAllChecked: {
              get() {
                  return this.completeSize===this.todos.length && this.todos.length>0;
              }
              set(value) { //当前checkbox最新的值
                 this.selectAllTodos(value);
              }
          }
      }
  }
</script>
<template>
  <div>
    <label>
      <input type="checkbox" v-model="isAllChecked"/>
    </label>
    <span>
      <span>已完成{{completeSize}}</span>/全部{{todos.length}}
    </span>
    <button v-show="completeSize>0" @click="deleteCompleteTodos">
        清除已经完成的任务
    </button>
  </div>
</template>
```

在App.vue中修改

```vue
<div>
    <todo-footer :todos="todos" :deleteCompleteTodos="deleteCompleteTodos" :selectAllTodos="selectAllTodos"/>
</div>

<script>
export default {
    methods: {
        //删除所有选中的
        deleteCompleteTodos() {
            this.todos = this.todos.filter(todo => !todo.complete)
        },
        //全选或者全不选
        selectAllTodos(check) {
            this.todos.forEach(todo => todo.complete=check);
        }
    }
}
</script>
```

### 存储数据

使用localstorage，并监视todos的改变

修改App.vue

```vue
<script>
  export default {
      return {
          //从localstorate读取todos, localstorage存的是json
          // 在chrome->application->localstorage可以看到
          todos: JSON.parse(window.localstorage.getItem('todos_key') || '[]');
      }，
      watch: { //深度监视
        todos: {
            deep: true, //表示深度
            handler: function(newValue, oldValue) {
                //将todos最新的json值，保存到localstorage
                window.localStorage.setItem('todos_key', JSON.stringify(newValue));
            }
        }    
      }
  }
</script>
```

### 自定义事件

- 绑定监听
- 触发

使用v-on:customname="custommethod"

或者用$on(eventName, callbackfunc)监听事件

使用$emit(eventName, data)触发事件

在上述示例中，App.vue

```vue
<template>
  <div>
    <!-- 给TodoHeader传一个函数-->
    <!--TodoHeader :addTodo="addTodo"/-->
    <!--采用事件的方式传递函数，绑定addTodo/-->
    <TodoHeader @addTodo="addTodo" ref="header"/>
  </div>
</template>
```

也可以在mounted里，通过js代码绑定监听

```javascript
export default {
    mounted() {
        // 执行异步代码
        // 给TodoHeader绑定事件监听
        // 引用标签里面的ref
        this.$refs.header.$on('addTodo', this.addTodo);
        
    }
}
```



同时去掉TodoHeader里面的props，同时更新TodoHeader.vue里的addItem()函数

```javascript
export default {
    methods {
          addItem() {
              // 1.检查合法性
              const title = this.title.trim();
              // 2.生成todo
              if(!title) {
                  alert("必须输入");
                  return;
              }
              const todo = {
                  title,
                  complete: false
              }
              // 3.添加到todos
              // this.addTodo(todo);
              // 触发自定义事件：addTodo
              this.$emit("addTodo", todo);
              // 4.清除输入
              this.title = '';
}
```

但是事件只能父传给子，不能像传函数一样，逐级传到descendent

### 消息订阅和发布

使用npm i pubsub-js

绑定事件监听-订阅消息

触发事件-发布消息

在App.vue上修改

```javascript
import PubSub from 'pubsub-js';
export default {
    mounted() {
        // 订阅消息
        PubSub.subscribe('deleteTodo', (msg, index)=>{
            //注意如果用function定义，this引用的是function
            this.deleteTodo(index);
        });
    }
}
```

在TodoItem.vue中发布消息

```javascript
import PubSub from 'pubsub-js';
export default {
    methods: {
        deleteItem() {
            //...
            // deleteTodo(index);
            PubSub.publish('deleteTodo', index);
        }
    }
}
```

### 数据存储优化

在src/util/storageUtil.js

```javascript
/*
使用localStorage存储数据的工具
1. 函数
2. 对象
需要向外暴露1个功能还是多个功能
如果1个是函数，如果多个采用对象

*/
const TODO_KEY='todos_key';
export default {
    saveTodos(todos) {
        window.localStorage.setItem(TODO_KEY, JSON.stringify(todos));
    }，
    readTodos() {
        return JSON.parse(window.localstorage.getItem(TODO_KEY || '[]');
    }
}
```

在App.vue中引用

```javascript
import storageUtil from './util/storageUtil'
export default {
    data() {
        return {
            todos: storageUtil.readTodos()
        }
    },
    watch: {
        todos: {
            deep: true,
            /*handle: function(value) {
                storageUtil.saveTodos(value);
            }*/
            handler: storageUtil.saveTodos
        }
    }
}
```

### ajax请求

- vue-resource：使用少
- axios：官方推荐，在2.x支持

在App.vue

```vue
<template>
  <div>
     <div v-if="!repoUrl">
         loading
    </div>   
     <div v-else>
         most star repo is <a :href="repoUrl">{{repoName}}</a>
    </div>
  </div>
</template>
<script>
  export default {
      data() {
          return {
            repoName: "",
            repoUrl: ""
          }
      },
      mounted() {
          //发ajax请求
          const url = "https://api.github.com/search/xxx";
          this.$http.get(url).then(
            response => {
                // 成功了
                const result = response.data;
                // 得到第一个repo
                const mostRepo = result.items[0];
                this.repoUrl = mostRepo.html_url;
                this.repoName = mostRepo.name;
            },
            response => {
                alert("请求失败");
            }
          );
      }
      
  }
</script>
```

在main.js中引入

```javascript
import VueResource from 'vue-resource'
//声明使用插件
Vue.use(VueResource) //内部会给vm对象和组件对象添加属性：$http
```

还有一种办法，就是直接在App.vue引入axios

```javascript
import axios from 'axios'
export default {
    mounted() {
        const url = "https://api.github.com/search/xxx";
        axios.get(url).then(response => {
            // 成功了
            const result = response.data;
            // 得到第一个repo
            const mostRepo = result.items[0];
            this.repoUrl = mostRepo.html_url;
            this.repoName = mostRepo.name;
        }).catch(error=> {
           alert("请求失败"); 
        });
    }
}
```

### 初始化显示

### 搜索

并排两个components，采用发布/订阅来传递消息

```vue
<template>
  <div>
    <input type="text" v-model="searchName"/>
    <button @click="search">
        search
    </button>    
  </div>
</template>
<script>
  import PubSub from "pubsub-js"
  export default {
      data() {
          return {
              searchName: ''
          }
      },
      methods: {
          search() {
              const searchName = this.searchName.trim();
              if(searchName) {
                  // 发布搜索的消息
                  PubSub.publish("search", searchName);
              }
          }
      }
  }
</script>
```

在main.vue中订阅消息

```javascript
mounted() {
    //订阅搜索的消息
    PubSub.subscribe("search", (msg, searchName) => {
       //需要发ajax请求
        const url = "xxx/users?q=${searchName}";
        // 更新状态
        this.users = null;
        this.errorMsg = '';
        // 发ajax请求
        axios.get(url).then(response => {
          const result = response.data;
          const users = result.items.map(item => ({
              url: item.html_url,
              avatar_url: item.avatar_url,
              name: item.login
          }));
          this.loading = false;
          this.users = user;
        }).catch(error => {
           this.loading = false;
           this.errorMsg = "请求失败";
        });
    });
}
```



### mint UI

vs. element ui:pc端

npm i --save mint-ui

可以按需打包，在.babelrc里面配置

```json
{
  "presets": [
    ["es2015", { "modules": false }]
  ],
  "plugins": [["component", [
    {
      "libraryName": "mint-ui",
      "style": true
    }
  ]]]
}
```

> 见帮助

组件包括：

- 标签组件
- 非标签组件

在index.html里面

```html
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no"/>
```

在main.js里

 ```javascript
import Vue from 'vue'
import App from './App.vue'
import {Button} from 'mint-ui'

// 注册成标签(全局)
// Button.name='mt-button'
Vue.component(Button.name, Button)


 ```

在App.vue里

```vue
<template>
  <mt-button type="primary" @click.native="handleClick">Test</mt-button>
</template>
<script>
  import {Toast} from 'mint-ui'
  export default {
      methods: {
          handleClick() {
              Toast("hello world");
          }
      }
  }
</script>
```

## 路由

### 简介

使用vue-router

```bash
npm i --save vue-router
```

路由器

路由：key->value，映射关系。key: path， value：前台的组件。path, component

步骤

1. 创建路由器``new VueRouter({})``
2. 创建路由``routes:[{path:xx, component:xx}]``
3. 注册路由器``new Vue({router})``
4. 使用路由标签组件
   1. ``<router-link>``生成路由链接
   2. ``<router-view>``：显示当前路由组件界面

### 基本路由

路由组件一般放在views, pages下。

非路由组件一般在components下。

![router-eg-1](router-eg-1.png)

![router-eg-2](router-eg-2.png)

有两个路由组件，about和home。

创建views/about.vue，views/home.vue。

创建router/index.js，定义路由器

```javascript
/*
路由器模块
*/
import Vue from 'vue'
import VueRouter from 'vue-router'

import About from '../views/About.vue'
import Home from '../views/Home.vue'

Vue.use(VueRouter)
// default表示默认暴露，可以以任何名字引用
export default new VueRouter({
   // n个路由
   routes: [
       {
           path: '/about',
           component: About
       },
       {
           path: '/home',
           component: Home
       },
       // 默认路由，自动加载
       {
           path: '/',
           redirect: '/about'
       }
   ]
});
```

在main.js里配置

```javascript
import Vue from 'vue'
import App from 'App.vue'
import router from './router'

new Vue({
    //配置对象属性名
    el: '#app',
    components: {App},
    template: '<App/>',
    // router: router
    router
})
```

![router-eg-design](router-eg-design.png)

在App.vue中

```vue
<template>
  <div>
    <div class="row">
        <div class="col-xs-offset-2 col-xs-8">
          <div class="page-header">
            <h2>
              Router-Basic -01    
            </h2>    
          </div>    
        </div>
    </div>
      
    <div class="row">
        <div class="col-xs-2 col-xs-offset-2">
          <div class="list-group">
            <!--和router/index.js必须一致 -->
            <router-link to="/about">About</router-link>
            <router-link to="/home">About</router-link>
          </div>    
        </div>
        <div class="col-xs-6">
          <div class="panel">
            <div class="panel-body">
              <router-view></router-view>    
            </div>    
          </div>    
        </div>
    </div>
  </div>
</template>

<script>
  export default {
      
  }
</script>
```

可以看到选中哪个router-link，对应的选中的标签就会有.router-link-active，可以给这个class加样式。

### 嵌套路由

在上图中，home里面也有两个链接，news和message

views/news.vue

```vue
<template>
  <div>
    <ul>
        <li v-for="(news, index) in newsArr" :key="index">{{news}}</li>
    </ul>    
  </div>
</template>

<script>
  export default {
      data() {
          return {
              newsArr: ['news001','news002','news003']
          }
      }
  }
</script>
```





views/message.vue

```vue
<template>
  <ul>
    <li v-for="(message, index) in messages" :key="message.id">
      <a href="???">{{message.title}}</a>
    </li>    
  </ul>
</template>

<script>
  export default {
      data() {
          return {
              messages: []
          }
      },
      
      mounted() {
          //模拟ajax请求从后台获取数据
          setTimeout(()=> {
              const messages = [
                  {
                      id: 1,
                      title: 'message001',
                      
                  },
                  {
                      id: 2,
                      title: 'message002',
                  },
                  {
                      id: 4,
                      title: 'message004'
                  }
                  
              ];
              this.messages = messages;
          }, 1000);
      }
  }
</script>
```

注册路由router/index.js

```javascript
//....
import About from '../views/About.vue'
import Home from '../views/Home.vue'
import News from '../views/news.vue'
import Message from '../views/message.vue'

export default new VueRouter({
   // n个路由
   routes: [
       {
           path: '/about',
           component: About
       },
       {
           path: '/home',
           component: Home,
           children: [
               {
                   //path: '/news', //path最左侧的/永远代表根路径
                   path: '/home/news', //采用全路径
                   component: News
               },
               {
                   path: 'message', //采用相对路径
                   component: Message
               },
               //默认/home的显示
               {
                   path: "",
                   component: News
               }
           ]
       },
       // 默认路由，自动加载
       {
           path: '/',
           redirect: '/about'
       }
   ]
});
```

在home.vue里显示相应组件

```vue
<template>
  <div>
    <ul class="nav nav-tabs">
      <li>
          <router-link to="/home/news">News</router-link>
          <router-link to="/home/message">Message</router-link>
      </li>     
    </ul>
    <div>
        <router-view></router-view>
    </div>
  </div>
</template>
<script>
  
</script>
```



### 缓存路由组件

正常情况下，一个路由组件被切换时，vue即会死亡。可以在``<router-view>``外面加上``<keep-alive>``使其**管理的路由组件**存活。

### 向路由组件传递数据

增加路由组件

创建views/MessageDetail.vue

```vue
<template>
  <div>
    <!--从chrome vue devtools里可以查到数据 -->
    <p>
        ID: {{$route.params.id}}
    </p>
    <ul>
        <li>id: {{messageDetail.id}}</li>
        <li>title: {{messageDetail.title}}</li>
        <li>content: {{messageDetail.content}}</li>
    </ul>
  </div>
</template>

<script>
export default {
      data() {
          return {
              messageDetail: {}
          }
      },
      mounted() {
          setTimeout(()=>{
                const allMessageDetails = [
                  {
                      id: 1,
                      title: 'message001',
                      content: 'messge001 content...'
                  },
                  {
                      id: 2,
                      title: 'message002',
                      content: 'messge002 content...'
                  },
                  {
                      id: 4,
                      title: 'message004',
                      content: 'messge004 content...'
                  },
                ];
              this.allMessageDetails = allMessageDetails;
              const id = this.$route.params.id * 1;//可强制转数字
              this.messgaeDetail = allMessageDetails.find(detail => detail.id==id);
          });
      },
    watch() {
        // 必须要监视$route的改变，否则MessageDetail的id只会更新一次，因为是在mounted里面执行的
        $route: function(value) {
            const id = value.params.id * 1;
            this.messgaeDetail = this.allMessageDetails.find(detail => detail.id==id);
        }
    }
  }
</script>
```

在router/index.js增加

```javascript
import MessageDetail from '../views/MessageDetail'

//...
export default new VueRouter({
   // n个路由
   routes: [
       {
           path: '/about',
           component: About
       },
       {
           path: '/home',
           component: Home,
           children: [
               {
                   //path: '/news', //path最左侧的/永远代表根路径
                   path: '/home/news', //采用全路径
                   component: News
               },
               {
                   path: 'message', //采用相对路径
                   component: Message,
                   children: [
                       {
                           path: 'detail/:id', //注意这里id是动态变化的
                           component: MessageDetail
                       }
                   ]
               },
               //默认/home的显示
               {
                   path: "",
                   component: News
               }
           ]
       },
       // 默认路由，自动加载
       {
           path: '/',
           redirect: '/about'
       }
   ]
});
```

修改Message.vue，**注意一个template下只能有一个子标签**

```vue
<template>
  <div>
        <ul>
          <li v-for="(message, index) in messages" :key="message.id">
            <a href="???">{{message.title}}</a>
            <!--注意es6的语法 -->
            <router-link :to="`/home/messgae/detail/${message.id}`">{{message.title}}</router-link>
          </li>    
        </ul>
        <hr/> <!--分割线 -->
        <router-view></router-view>
  </div>
</template>
```

也可以通过$route.query带参数。参数是GET参数形式。

另一种是通过``<router-view>``属性携带参数。比如在App.vue上传递数值

```html
<router-view :msg="abc"></router-view>
```

在About.vue上就可以访问到该属性。

```vue
<script>
  export default {
      props: {
          msg: String
      }
  }
</script>

<template>
  <div>
    <p>
        {{msg}}
    </p>    
  </div>
</template>
```

### 编程式路由导航

通过js实现跳转

```js
window.location.href = url;
```

或者用

```javascript
//影响回退
this.$router.push("/"); //压入栈头
this.$router.replace("/"); //替换栈头
this.$router.back(); //回退
```

## Vue源码分析

### 说明

github上的mvvm，仿照vue的mvvm的原理

4个js文件

prepare.html

```html
<script>
  //1.[].slice.call(lis)：将伪数组转换为真数组
  const lis = document.getElementsByTagName('li'); //伪数组
  console.log(lis instanceof Array, //false
              lis[1].innerHTML,  // returns
             lis.forEach); // undefined
  const lis2 = Array.prototype.slice.call(lis); // call让函数成为
  console.log(lis2 instance of Array, // true
             lis2[1].innerHTML, // returns
             lis2.forEach); // function
  //2. node.nodeType：得到节点类型
  // 由大到小分别为document->element->attr->text
  const elementNode = document.getElementById("test");
  const attrNode = elementNode.getAttributeNode("id");
  const textNode = elementNode.firstChild;
  console.log(elementNode.nodeType, 
              attrNode.nodeType, 
              textNode.nodeType);
  //3. Object.defineProperties(obj, propertyName, {})：给对象添加属性
  const obj = {
      firstName: 'A',
      lastName: 'B'
  }
  // 给obj添加fullname属性
  Object.defineProperty(obj, 'fullName', {
     // configurable：是否可以重新定义
      // enumerable: 是否可以枚举
      // value: 初始值
      // writable：是否可以修改属性值
      // get: 回调函数，根据其它相关属性动态初始计算得到当前属性值
      // set：回调函数，监视当前属性值的变化，更新其它相关的属性值
      get() {
          return this.firstName + "-" + this.lastName;
      },
      set(value) {
          const names = value.split('-');
          this.firstName = names[0];
          this.lastName = names[1];
      }
      /**
      数据描述符
        configurable
        enumerable
        value
        writable
      访问描述符
       get
       set
      **/
  });
  console.log(obj.fullName);
  obj.firstName = 'C'
  obj.lastName = 'D'
  console.log(obj.fullName);
  obj.fullName = 'E-F';
  console.log(obj.firstName + "," + obj.lastName)
  Object.defineProperty(obj, 'fullName2', {
    configurable: false,
    enumerable: true,
    value: 'G-H',
    writable: false
  });
  console.log(obj.fullName2);
  obj.fullName2 = 'J-K'; // not successful
  // 4. Object.keys(obj):得到对象自身可枚举属性组成的数组
  const names = Object.keys(obj); //得到firstName, lastName, fullName2，但没有fullName
  // 3.4.特性不支持IE8，也就是IE8不支持ES5语法
  
  // 5. obj.hasOwnProperty(prop)：判断prop是否是obj自身的属性
  console.log(obj.hasOwnProperty('fullName'), obj.hasOwnProperty('toString'));//true,false
  
  // 6. DocumentFragment：和document, element相同，也是一种Node
  // 可以高效批量更新多个节点
  // document: 对应显示的页面，包含n个element,一旦更新document内部某个元素，界面更新。如果对<li>n个node进行更新，会导致界面更新n次。
  // documentFragment：内存中保存n个element的容器（不与界面关联），如果更新fragment的某一个element,界面不变
    
  const ul = document.getElementById("fragment_test");
  // 1.创建fragment
  const fragment = document.createDocumentFragment();
    
  // 2.取出ul所有子节点,注意换行本身也是node
  let child
  while(child = ul.firstChild) {
      // 一个节点只能有一个父亲
      // 所以就是先将child从ul移除，添加为fragment子节点
      fragment.appendChild(child)   
  }
    
  // 3. 更新fragment中所有的li的文本
  Array.prototype.slice.call(fragment.childrenNodes).forEach(node => {
     if(node.nodeType == 1) { //元素节点<li>
         node.textContext = "ddddd"
     } 
  });
  // 4. 将fragment插入ul
  ul.appendChild(fragment);
  
</script>
<div id="test">
    ddddd
</div>
<ul id="fragment_test">
    <li>1</li>
    <li>2</li>
</ul>
```

### 数据代理

通过一个对象代理对另一个对象中属性的读写

a.b.xxx等价于a.xxx

示例文件

```html
<script src="js/vue.js"></script>
<script>
  const vm = new Vue({
     el: '#test',
     data: {
         name: "feifei"
     }
  });
  console.log(vm.name); // 实际访问的是vm.data.name，代理读
  vm.name = 'xiaoxiao'
  console.log(vm._data.name) // xiaoxiao，代理写
  
  
</script>
<div id="test">
    
</div>
```

采用MVVM示例，改为

```html
<script src="./js/observer.js"></script>
<script src="./js/watcher.js"></script>
<script src="./js/compile.js"></script>
<script src="./js/mvvm.js"></script>
<script>
  const vm = new MVVM({
     el: '#test',
     data: {
         name: "feifei"
     }
  });
  console.log(vm.name); // 实际访问的是vm.data.name，代理读
  vm.name = 'xiaoxiao'
  console.log(vm._data.name) // xiaoxiao，代理写
  
  
</script>
<div id="test">
    
</div>
```

注意到

```javascript
function MVVM(options) {
    // 将配置对象保存到vm
    this.$options = options || {};
    
    // 将data保存到vm和变量_data
    var data = this._data = this.$options.data;
    // 保存vm到me
    var me = this;

    // 数据代理
    // 实现 vm.xxx -> vm._data.xxx
    Object.keys(data).forEach(function(key) {
        // 对指定的属性实现代理
        me._proxyData(key);
    });
    
    //……
}

MVVM.prototype = {
    constructor: MVVM,
    // ……
    
    //实现指定属性代理的方法

    _proxyData: function(key, setter, getter) {
        var me = this;
        setter = setter || 
        // 给vm指定属性名的属性（使用属性描述符）
        Object.defineProperty(me, key, {
            configurable: false,//防止修改
            enumerable: true,//可以枚举遍历
            //当通过vm.xxx读取属性值时调用
            get: function proxyGetter() {
                return me._data[key];
            },
            //当通过vm.xx=value时，value被保存到data对应的属性上
            set: function proxySetter(newVal) {
                me._data[key] = newVal;
            }
        });
    }
}
```



### 模板解析

```html
<div id="test">
    <p>
        {{name}}
    </p>
</div>

<script src="./js/observer.js"></script>
<script src="./js/watcher.js"></script>
<script src="./js/compile.js"></script>
<script src="./js/mvvm.js"></script>
<script>
  new MVVM({
     el: "#test",
     data: {
         name: "feifei"
     }
  });
</script>
```

再查看对应的mvvm.js

```javascript
  
function MVVM(options) {
    //...
    this._initComputed();

    observe(data, this);
    // 创建编译对象，处理模板
    this.$compile = new Compile(options.el || document.body, this)
}
```

再找对应的compile.js

```javascript
function Compile(el, vm) {
    this.$vm = vm;
    // 将el对应的元素对象保存到compile对象中
    this.$el = this.isElementNode(el) ? el : document.querySelector(el);
    
    // 如果有，才进行
    // 模板解析，通过documentfragment先在内存中替换好了，再塞回去
    if (this.$el) {
        //取出el元素中所有的子节点保存到fragment
        this.$fragment = this.node2Fragment(this.$el);
        // 编译fragment中所有层次的子节点
        this.init();
        // 将编译好的fragment添加到页面的el元素中
        this.$el.appendChild(this.$fragment);
    }
}

Compile.prototype = {
    constructor: Compile,
    node2Fragment: function(el) {
        // 创建空的fragment
        var fragment = document.createDocumentFragment(),
            child;

        // 将el中原生节点剪切到fragment
        while (child = el.firstChild) {
            fragment.appendChild(child);
        }

        return fragment;
    },

    init: function() {
        //编译指定元素（所有层次的子节点）
        this.compileElement(this.$fragment);
    },

    compileElement: function(el) {
        //取出最外层所有的子节点
        var childNodes = el.childNodes
            // 保存compile
            me = this;
        // 遍历所有的子节点（text/element）
        [].slice.call(childNodes).forEach(function(node) {
            // 得到节点的文本内容
            var text = node.textContent;
            var reg = /\{\{(.*)\}\}/; //采用子匹配
            
            // 处理元素节点（解析指令）
            if (me.isElementNode(node)) {
                me.compile(node);
                // 判断节点是否是大括号格式的文本节点
            } else if (me.isTextNode(node) && reg.test(text)) {
                // 在这里能处理到{{name}}文本， RegExp.$1匹配（）里的文本
                me.compileText(node, RegExp.$1.trim());
            }
            
            
            // 如果当前节点还有子节点，
            // 采用递归将所有的层次都遍历一次
            if (node.childNodes && node.childNodes.length) {
                me.compileElement(node);
            }
        });
    }
    
    compile: function(node) {
        var nodeAttrs = node.attributes,
            me = this;

        [].slice.call(nodeAttrs).forEach(function(attr) {
            var attrName = attr.name;
            if (me.isDirective(attrName)) {
                var exp = attr.value;
                var dir = attrName.substring(2);
                // 事件指令
                if (me.isEventDirective(dir)) {
                    compileUtil.eventHandler(node, me.$vm, exp, dir);
                    // 普通指令
                } else {
                    compileUtil[dir] && compileUtil[dir](node, me.$vm, exp);
                }

                node.removeAttribute(attrName);
            }
        });
    },

    compileText: function(node, exp) {
        // 注意调用的是text
        compileUtil.text(node, this.$vm, exp);
    },    
    
    //...
    
}

// 包含多个解析指令的方法的工具对象
var compileUtil = {
    text: function(node, vm, exp) {
        this.bind(node, vm, exp, 'text');
    },
    
    // 解析v-html
    // 解析v-model
    // 解析v-class
    // 最后都用bind去做
    
    bind: function(node, vm, exp, dir) {
        // 得到更新节点的函数
        var updaterFn = updater[dir + 'Updater'];
        // 调用函数更新节点
        updaterFn && updaterFn(node, this._getVMVal(vm, exp));

        new Watcher(vm, exp, function(value, oldValue) {
            updaterFn && updaterFn(node, value, oldValue);
        });
    },
    
    // 从vm得到表达式（exp）所对应的值
    // 注意exp有可能是a.b.c的形式，因此需要逐级取真正的属性值
    _getVMVal: function(vm, exp) {
        var val = vm;
        exp = exp.split('.');
        exp.forEach(function(k) {
            val = val[k];
        });
        return val;
    },

}

// 包含多个更新节点的方法的工具对象
var updater = {
    // 更新节点的text
    textUpdater: function(node, value) {
        node.textContent = typeof value == 'undefined' ? '' : value;
    }
    
    //...
}

```

总而言之，对``<p>{{name}}</p>``的处理方法是：

1. 取name
2. 取name对应的value
3. 更新``<p/>``里的textContent

### 事件指令解析



```html
<div id="test">
    <p>
        {{name}}
    </p>
    <button v-on:click="show">
        提示
    </button>
</div>

<script src="./js/observer.js"></script>
<script src="./js/watcher.js"></script>
<script src="./js/compile.js"></script>
<script src="./js/mvvm.js"></script>
<script>
  new MVVM({
     el: "#test",
     data: {
         name: "feifei"
     },
     methods: {
         show() {
             alert(this.name);
         }
     }
  });
</script>
```

直接看compile.js中的这段代码

```javascript
// 处理元素节点（解析指令）
if (me.isElementNode(node)) {
    me.compile(node);
    // 判断节点是否是大括号格式的文本节点
}
```

看compile

```javascript
compile: function(node) {
    var nodeAttrs = node.attributes,
        me = this;
    // 遍历所有的属性
    [].slice.call(nodeAttrs).forEach(function(attr) {
        var attrName = attr.name; //v-on:click属性
        // attr.indexOf("v-") == 0
        if (me.isDirective(attrName)) {
            var exp = attr.value; // show
            var dir = attrName.substring(2); // on:click
            // 事件指令, attr.indexOf("on")==0
            if (me.isEventDirective(dir)) {
                // 事件指令处理
                compileUtil.eventHandler(node, me.$vm, exp, dir);
                // 普通指令
            } else {
                compileUtil[dir] && compileUtil[dir](node, me.$vm, exp);
            }
            // 移除指令属性v-on:click
            node.removeAttribute(attrName);
        }
    });
},
```
看compileUtil

```javascript
// 指令处理集合
var compileUtil = {
    // 事件处理
    eventHandler: function(node, vm, exp, dir) {
        var eventType = dir.split(':')[1], // click
            fn = vm.$options.methods && vm.$options.methods[exp]; //取得show的fn

        if (eventType && fn) {
            node.addEventListener(eventType, fn.bind(vm), false); // 注意不是直接传fn，而是要将fn中的this强制=vm
        }
    },
    //...
}
```



### 一般指令

```html
<div id="test">
    <p v-text="msg">
        
    </p>
    <p v-html="msg">
        
    </p>
    <p v-class="myClass">
        不快乐
    </p>
</div>

<script src="./js/observer.js"></script>
<script src="./js/watcher.js"></script>
<script src="./js/compile.js"></script>
<script src="./js/mvvm.js"></script>
<script>
  new MVVM({
     el: "#test",
     data: {
         msg: "<a href='http://www.baidu.com'>百度</a>",
         MyClass: "stylename"
     }
  });
</script>
```

看compile.js中的

```javascript
compileUtil[dir] && compileUtil[dir](node, me.$vm, exp);
```

```javascript
// 指令处理集合
var compileUtil = {
    text: function(node, vm, exp) {
        this.bind(node, vm, exp, 'text');
    },

    html: function(node, vm, exp) {
        this.bind(node, vm, exp, 'html');
    },
    
    
    class: function(node, vm, exp) {
        this.bind(node, vm, exp, 'class');
    },

    bind: function(node, vm, exp, dir) {
        var updaterFn = updater[dir + 'Updater'];

        updaterFn && updaterFn(node, this._getVMVal(vm, exp));

        new Watcher(vm, exp, function(value, oldValue) {
            updaterFn && updaterFn(node, value, oldValue);
        });
    },
    
    //...
}

var updater = {
    textUpdater: function(node, value) {
        node.textContent = typeof value == 'undefined' ? '' : value;
    },

    htmlUpdater: function(node, value) {
        node.innerHTML = typeof value == 'undefined' ? '' : value;
    },

    classUpdater: function(node, value, oldValue) {
        // 原<p>也会有静态的className，要和v-class合并
        var className = node.className;
        className = className.replace(oldValue, '').replace(/\s$/, '');

        var space = className && String(value) ? ' ' : '';

        node.className = className + space + value;
    }
    
    //...
};
```



### 数据绑定与劫持

一旦更新了data某个属性值，所有界面上直接使用或者间接使用了此属性的节点都会更新。

数据劫持是vue用来实现数据绑定的技术：通过``defineProperty()``监视data中所有属性数据的变化。一旦变化就去更新更新。

![mvvm](mvvm.png)

```html
<script src="./js/observer.js"></script>
<script src="./js/watcher.js"></script>
<script src="./js/compile.js"></script>
<script src="./js/mvvm.js"></script>
<script>
  new MVVM({
     el: '#test',
     data: {
         name: 'feife'
     },
     methods: {
         update() {
            this.name="xinxin" 
         }
     }
  });
</script>

<div id="test">
    <p><!-- w1-- d0 -->
        {{name}}
    </p>
    <p v-text="name"> <!-- w2-- d0 -->
        
    </p>
    <p v-text="wife.name"> <!-- w3 -- {d1,d2}-->
        
    </p>
    <!-- d0--[w1,w2] --> <!--d1--[w3]--><!--d2-[w3]-->
    <button v-on:click="update">
        更新
    </button>
</div>
```

注意在mvvm.js中，有

```javascript
observe(data, this)
```

在observer.js中，有

```javascript
// 对data中所有层次的属性通过数据劫持实现数据绑定
function observe(value, vm) {
    // 被观察的必须是一个对象
    if (!value || typeof value !== 'object') {
        return;
    }
    // 创建对应的observer
    return new Observer(value);
};
function Observer(data) {
    this.data = data; //保存dataq
    this.walk(data); //开始对data的监视
}

Observer.prototype = {
    constructor: Observer,
    walk: function(data) {
        var me = this;
        // 遍历所有的属性，也就是data.name
        Object.keys(data).forEach(function(key) {
            me.convert(key, data[key]); //处理k->v
        });
    },
    convert: function(key, val) {
        //实现数据响应式的绑定
        this.defineReactive(this.data, key, val);
    },
    defineReactive: function(data, key, val) {
        //创建属性对应的dep
        var dep = new Dep();
        //递归实现数据绑定，对data中所有层次的属性
        //对于data.name="feifei"，只会返回undefined
        var childObj = observe(val);
        // 给data重新定义get/set方法
        Object.defineProperty(data, key, {
            enumerable: true, // 可枚举
            configurable: false, // 不能再define
            get: function() { //建立dep与watcher之间的关系
                if (Dep.target) {
                    dep.depend(); //建立关系
                }
                return val;
            },
            set: function(newVal) { //监视key属性的变化
                if (newVal === val) {
                    return;
                }
                val = newVal;
                // 新的值是object的话，进行监听，即有可能是a={b:dd}，需要监视b
                childObj = observe(newVal);
                // 通知订阅者，更新界面
                dep.notify();
            }
        });
    }
}
```

注意到compile.js中的bind函数有这一句

```javascript
// 对每个事件或表达式exp，都有一个watch
// 发生变动就会执行回调函数
// 注意在事件指令时，不会走这个语句
new Watcher(vm, exp, function(value, oldValue) {
    updaterFn && updaterFn(node, value, oldValue);
});
```

再看watcher.js

```javascript
function Watcher(vm, expOrFn, cb) {
    this.cb = cb; //更新界面的回调
    this.vm = vm; 
    this.expOrFn = expOrFn; //表达式
    this.depIds = {}; //包含所有相关的dep的容器对象

    if (typeof expOrFn === 'function') {
        this.getter = expOrFn;
    } else {
        this.getter = this.parseGetter(expOrFn.trim());
    }

    this.value = this.get(); //这一步会创建dep，invoke 
}


```

Dep

- 创建的时候：初始化的给data的属性进行数据劫持时创建(defineReactive)
- 个数：与data的属性一一对应，包括非叶子属性
- Dep的结构：
  - id：标识（uid）
  - subs：n个相关的watcher的容器

watcher

- 创建的时候：初始化的compile大括号表达式/一般指令时创建
- 个数：与模板中表达式，如{{name}},v-text（非事件指令v-on)一一对应
- 结构
  - cb：更新界面的回调
  - vm
  - exp：表达式
  - depIds：相关的n个dep的容器对象
  - value：当前exp对应的value

> 若执行``vm.name='abc'``，导致data中name属性的变化，name的set()调用，``dep.notify()``，通知相关所有的watcher，调用``cb.updater``。

Dep与watcher的关系

关系：多对多的关系

> name --> Dep --> n个watcher：{{name}}, v-text="name"，即属性在模板中多次被使用。
>
> 表达式-->Watcher-->n个Dep：{{a.b}}对应a, a.b两个属性，即多层表达式

如何建立：注意上面函数中的get和set函数，调用了``dep.depend()``。详见以下的代码

回来看observer.js中的dep

```javascript
var uid = 0;

function Dep() {
    this.id = uid++;
    // 多个订阅者，用于放置watcher
    this.subs = [];
}

Dep.prototype = {
    // 添加watcher到dep中
    addSub: function(sub) {
        this.subs.push(sub);
    },
    
    //去建立dep与watcher的关系
    depend: function() {
        Dep.target.addDep(this); //这里dep.target=watcher
    },

    removeSub: function(sub) {
        var index = this.subs.indexOf(sub);
        if (index != -1) {
            this.subs.splice(index, 1);
        }
    },
    //遍历所有的watcher进行更新
    notify: function() {
        this.subs.forEach(function(sub) {
            sub.update(); //在这里通知watcher
        });
    }
};

Dep.target = null;
```

这样``depend``又会调用``watcher.addDep``

```javascript
Watcher.prototype = {
    constructor: Watcher,
    //上面代码dep.notify之后，表示视图即将要发生改变
    update: function() {
        this.run();
    },
    run: function() {
        var value = this.get();
        var oldVal = this.value;
        if (value !== oldVal) {
            this.value = value;
            this.cb.call(this.vm, value, oldVal);
        }
    },
    addDep: function(dep) {
        // 判断dep与watcher的关系是否已经建立
        if (!this.depIds.hasOwnProperty(dep.id)) {
            dep.addSub(this); //建立dep->watch的关系，用于更新
            this.depIds[dep.id] = dep; //建立watch->dep的关系，用于防止重复建立关系
        }
    },
    
    // 得到表达式对应的值，并建立dep与watcher的关系
    get: function() {
        Dep.target = this; //给dep指定当前的watcher
        // 调用parseGetter
        var value = this.getter.call(this.vm, this.vm);
        Dep.target = null; //关系建立了，就没必要再用了 
        return value;
    },
    // 得到表达式对应的值
    parseGetter: function(exp) {
        if (/[^\w.$]/.test(exp)) return; 

        var exps = exp.split('.');

        return function(obj) {
            for (var i = 0, len = exps.length; i < len; i++) {
                if (!obj) return;
                obj = obj[exps[i]];
            }
            return obj;
        }
    }
};
```



在上面的``addDep``函数中，会有操作``this.depIds[]=xx``，即建立了watcher和dep的关系。

回到之前的问题

dep与watcher的关系如何建立？

- data中属性的get()中建立

什么时候建立？

- 初始化的解析模板中的表达式时``new watcher()``

### 双向数据绑定

```html
<script>
  new MVVM({
     el: '#test',
     data: {
         msg: 'atguid'
     }
  });
</script>
<div id='test'>
    <input type="text" v-model="msg"/>
    <p>
        {{msg}}
    </p>
</div>
```

双向数据绑定

- input value - data.msg：看第一个
- data.msg - {{msg}}：<u>已经解决</u>

注意在**一般指令**里面，有个v-model指令的解析

```javascript
// 指令处理集合
var compileUtil = {
    //...

    model: function(node, vm, exp) {
        //实现初始化显示和创建watcher
        this.bind(node, vm, exp, 'model');

        var me = this,
            val = this._getVMVal(vm, exp);
        // 在输入过程中会发生
        node.addEventListener('input', function(e) {
            var newValue = e.target.value;
            if (val === newValue) {
                return;
            }
            // 在这里会将值保存到data.msg中
            me._setVMVal(vm, exp, newValue);
            val = newValue;
        });
    },
    bind: function(node, vm, exp, dir) {
        var updaterFn = updater[dir + 'Updater'];

        updaterFn && updaterFn(node, this._getVMVal(vm, exp));
        // 创建对应的watcher
        // data.msg变化->updater.modelUpdater
        new Watcher(vm, exp, function(value, oldValue) {
            updaterFn && updaterFn(node, value, oldValue);
        });
    }
},
var updater = {
  //...
  modelUpdater: function(node, value, oldValue) {
        node.value = typeof value == 'undefined' ? '' : value; 
    }
}
```

## Vuex

### 非vuex的示例

App.vue

```vue
<template>
  <div>
    <p>
        click {{count}} times, count is {{evenOrOdd}}
    </p>
    <button @click="increment">+</button>
    <button @click="decrement">-</button>
    <button @click="incrementIfOdd">
        increment if odd
    </button>
    <button @click="incrementAsync">
       increment async
    </button>
  </div>
</template>

<script>
  export default {
      data() {
          return {
              count: 0
          }
      },
      computed: {
        evenOrOdd() {
            return this.count%2==0?"偶数":"奇数"
        }
      },
      methods: {
          increment() {
              let count = this.count
              this.count = ++count;
          },
          decrement() {
             let count = this.count
              this.count = --count; 
          },
          incrementIfOdd() {
              let count = this.count
              if(count % 2==1)
              	this.count = ++count;
          },
          incrementAsync() {
              setTimeOut(()=>{
                  let count = this.count
                  this.count = ++count
              }, 1000);
          }
      }
  }
</script>
```



### Vuex介绍

Vue的插件

状态自管理

- state-view-actions-state

- view：初始化显示，更新显示。以声明方式将state映射到视图

- state：读取状态

- actions：更新状态的一堆函数。

多组件共享状态问题

- 多个view依赖于同一个状态
- 来自不同view的行为需要变更同一个状态
- 之前的办法：将共享的数据定义在**父组件**

![](vuex.png)

actions不直接更新状态，通过mutations更新。

backend API：可以发ajax请求

<u>这也是vuex和纯vue的区别所在</u>

### 修改之前的vue版本

下载

```bash
npm i --save vuex
```

在main.js中引用

```javascript
import store from './store'
new Vue({
    el: '#app',
    components: {App},
    template: '<App/>',
    store //所有组件对象都多了一个属性$store
})
```



创建src/store.js

```javascript
/*
vue的核心管理对象模块
*/
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = { //初始化状态
    count: 0
}

const mutations = {
    // 增加的mutation
    INCREMENT(state) {
        state.count ++
    }
    // 减少的mutation
    DECREMENT(state) {
        state.count --
    }
}

const actions = {
    //增加的action
    increment({commit}) {
        //提交mutation
        commit('INCREMENT')
    },
    
    decrement({commit}) {
        //提交mutation
        commit('DECREMENT')
    },
    incrementIfOdd({commit, state}) {
        if(state.count%2===1) {
            commit('INCREMENT')        
        }
    },
    incrementAsync({commit, state}) {
        // 在action直接可以执行异步代码
        setTimeOut(()=>{
            commit('INCREMENT')            
        }, 1000)
    }
    
    
}

const getters = {
    evenOrOdd(state) { //注意不需要调用，只需要读取属性值
        return state.count%2===0?'偶数':'奇数'
    }
}


export default new Vuex.Store({
 state,// 状态
 mutations, //包含多个更新state函数的对象
 actions, //包含多个对应的事件回调函数的对象
 getters //包含多个getter计算属性函数的对象
})

```

更新App.vue

```vue
<template>
  <div>
    <p>
        click {{$store.state.count}} times,
        count is {{evenOrOdd}}
    </p>    
  </div>
</template>
<script>
 export default {
     computed: {
         evenOrOdd() {
             // 注意这里返回的是属性，不需要再显示的计算
             return this.$store.getters.evenOrOdd
         }
     },
     methods: {
          increment() {
              // 通知vuex去增加
              this.$store.dispatch('increment')
          },
          decrement() {
              this.$store.dispatch('decrement')
          },
          incrementIfOdd() {
               this.$store.dispatch('incrementIfOdd')     
          },
          incrementAsync() {
               this.$store.dispatch('incrementAsync')     
          }
      }
 }
</script>
```

### 优化代码

在App.vue中

```vue
<script>
  import {mapState, mapGetters, mapActions} from 'vuex'
  
  export default {
      //上面代码中，computed都会引用this.$store.xxx属性，可以简化
      computed: {
          // 和下面mapGetters的原理相似
          ...mapState(['count']),
          // mapGetters()返回值：
          // {evenOrOdd(){return this.$store.getters['evenOrOdd']}
          // 也可以写作
          // ...mapGetters({evenOrOdd: 'evenOrOdd'})
          // 后一个参数表示在store.js定义的名字
          // 前一个参数表示在当前页面引用的名字
          ...mapGetters(['evenOrOdd']),
      },
      methods: {
          ...mapActions(['increment',
                        'decrement',
                        'incrementIfOdd',
                        'incrementAsync'])
      }
  }
</script>

```



### todolist应用

store/index.js

```javascript
import Vue from 'vue'
import Vuex from 'vuex'
import state from './state'
import mutations from './mutations'
import actions from './actions'
import getters from './getters'

Vue.use(Vuex)
export default new Vuex.Store({
    state,
    mutations,
    actions,
    getters
})
```

main.js

```javascript
import Vue from 'vue'
import App from './components/App.vue'
import store from './store' //默认引入'./store/index'

new Vue({
  el: '#app',
  render: h=>h(App),
  store
})

```

在App.vue中

```vue
<template>
  <div class="todo-container">
    <div class="todo-wrap">
        <todos-header/>
        <List/>
        <todos-footer/>
    </div>    
  </div>
</template>
<script>
  import Header from './Header.vue'
  import List from './List.vue'
  import Footer from './Footer.vue'
  import storageUtil from '../util/storageUtil'
    
  export default {
      
  }
</script>
```

Header.vue

```vue
<template>
  <div class="todo-header">
    <input type="text" placeholder="输入名称" v-model="inputTodo" @keyup.enter="add"/>    
  </div>
</template>
<script>
  export default {
      data() {
          return {
              inputTodo: '' //组件内部使用的状态，不存在共享，不需要vuex
          }
      }，
      methods: {
        add() {
            const inputTodo = this.inputTodo.trim()
            if(!inputTodo) {
                return
            }
            const todo = {
                title: inputTodo,
                complete: false
            }
            // 通知vuex
            this.$store.dispatch('addTodo', todo)
            
            // 清除
            this.inputTodo = ''
            
        }
      }
  }
</script>
```

store/mutation-types.js：定义常量

```javascript
export const ADD_TODO = 'add_todo'
export const DELETE_TODO = "delete_todo"
export const SELECT_ALL_TODOS = "selectAllTodos"

```



store/state.js

```javascript
export default {
    todos: []
}
```

store/actions.js

```javascript
import {ADD_TODO, DELETE_TODO,SELECT_ALL_TODOS} from './mutation-types'
export default {
    addTodo({commit}, todo) {
        commit(ADD_TODO, {todo})
    },
    deleteTodo({commit}, index) {
        commit(DELETE_TODO, {index})
    },
    selectAllTodos({commit}, check) {
        commit(SELECT_ALL_TODOS, {check})
    }
}
```

store/mutations.js

```javascript
import {ADD_TODO, DELETE_TODO,SELECT_ALL_TODOS} from './mutation-types'
export default {
    [ADD_TODO](state, {todo}) {
        state.todos.unshift(todo)
    },
    [DELETE_TODO](state, {index}) {
        state.todos.splice(index, 1)
    },
    [SELECT_ALL_TODOS](state, {check}) {
        state.todos.forEach(todo => todo.complete = check)
    }
}
```

components/List.vue

```vue
<template>
  <ul>
    <item v-for="(todo, index) in todos" :key="index" :todo="todo" :index="index"></item>    
  </ul>
</template>
<script>
  import item from './item.vue'
  import {mapState} from 'vuex'
  
  export default {
      components: {
          item
      },
      computed: {
          ...mapState(['todos'])
      }
  }
</script>
```

components/Item.vue

```vue
<script>
  export default {
      props: {
          todo: Object,
          index: Number
      },
      
      data() {
          isShow: false,
          bgColor: 'white'
      },
      
      methods: {
          deleteItem() {
              this.$store.dispatch('deleteTodo', index)
          }
      }
  }
</script>
```

store/getters.js

```javascript
/*
包含所有基于state的getter计算属性
*/
export default {
    totalCount(state) {
        return state.todos.length
    },
    completeCount(state) {
        return state.todos.reduce((preTotal, todo) => {
            return preTotal + (todo.complete ? 1:0)
        }, 0)
    },
    //是否全部选中，注意通过getters引用
    isAllSelected(state, getters) {
        return getters.totalCount===getters.completeCount
          && getters.totalCount > 0
    }
    //注意我们不能写setters，只能在components里面写
}
```





components/footer.vue

```vue
<script>
  import {mapGetters} from 'vuex'
    
  export default {
      computed: {
          ...mapGetters(['totalCount',
                       'completeCount']),
          //
          isAllComplete: {
              get() {
                  return this.$store.getters.isAllComplete
              },
              set(value) {
                  this.$store.dispatch('selectAllTodos', value)
              }
          }
      }
  }
</script>
```

### 缓存数据

更改App.vue

```javascript
export default {
    mounted() {
        //异步获取数据保存todos数据并显示
        //利用action
        this.$store.dispatch('reqTodos')
    }
}
```

在actions.js增加

```javascript
import storageUtil from '../utils/storageUtil'
export default {
    //...
    reqTodos({commit}) {
        // simulate
        setTimeout(() => {
			const todos = storageUtil.readTodos()
            // 提交
            // 注意在mutation-types里，要定义好这个变量
            commit(RECEIVE_TODOS, todos)
        }, 1000)
    }
}

```

在mutations.js里增加

```javascript
export default {
    //...
    [RECEIVE_TODOS](state, todos) {
        state.todos = todos
    }
}
```

现在要监视todos的变化

在Main.vue中

```javascript
export default {
    computed {
   		..mapState(['todos'])
	},
    watch: {
        todos: { 
            deep: true, //深度监视todos的所有变化
            handler: storageUtil.saveTodos
        }
    }
}
```

### 关于render配置

在main.js里

```javascript
new Vue({
    el: '#app',
    /*components: {
        App
    },
    template: '<App/>',*/
    render: h => h(App), //创建渲染，插入到el中
    /*
    function(createElement) {
    	return createElement(App) //<App/>
    }
    */
    store
})
```

